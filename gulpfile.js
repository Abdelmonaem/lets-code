// Config
const gulp = require('gulp');
const watch = require('gulp-watch');
const pug = require('gulp-pug');
const sass = require('gulp-ruby-sass');
const autoprefixer = require('gulp-autoprefixer');
const connect = require('gulp-connect');
const concat = require('gulp-concat');

//Tasks
// watch any changes in sass & pug files
gulp.task('watch', () => {
    gulp.watch('src/**/*.scss', ['sass']);
    gulp.watch('src/**/*.pug', ['pug']);
    gulp.watch('src/js/**/*.js', ['js-concat']);
});
// compile sass
gulp.task('sass', () => {
    sass('src/sass/style.scss', {
        sourcemap: true,
        style: 'compressed'
    })
    .on('error', sass.logError)
    .pipe(autoprefixer({
        browsers: ['last 2 versions'],
        cascade: false
    }))
    .pipe(gulp.dest('assets/css'))
    .pipe(connect.reload());
});

//compile pug
gulp.task('pug', () => {
    return gulp.src('src/pug/*.pug')
               .pipe(pug({
                    pretty: true 
                }))
                .pipe(gulp.dest('./'))
                .pipe(connect.reload());
});
// connect to local host
gulp.task('server', () => {
    connect.server({
        port: 5000,
        root: './',
        livereload: true
    })
});
// concat js files
gulp.task('js-concat', () => {
    return gulp.src(['src/js/vendors/*.js', 'src/js/partials/*.js'])
      .pipe(concat('scripts.js'))
      .pipe(gulp.dest('assets/js'))
      .pipe(connect.reload());
  });

// default task
gulp.task('default', ['server', 'watch']);