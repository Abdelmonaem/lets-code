$(document).on('click', '.toggle-parent', function(){
    var $this = $(this);
    $this.parent().toggleClass('active');
    if($this.parent('.accordion').length){
        $this.parent().siblings().removeClass('active').find('.accordion-body').slideUp(300);
        $this.parent().find('.accordion-body').slideToggle(300);
    }
});
$(document).on('click', function(e){
    var $target = $(e.target);
    $('.click-outside').each(function(index){
        var $this = $(this);
        var _hasClass = $this.hasClass('remove-from-parent') ? $this.parent().hasClass('active') : $this.hasClass('active');
        if (_hasClass && $target.closest('.click-outside').length == 0 && !$target.hasClass('click-outside-btn')) {
            $this.hasClass('remove-from-parent') ? $this.parent().removeClass('active').parents('body').removeClass('is-oppened') : $this.removeClass('active');
        }
    });
});