 $('.slider').slider();
 $('.product-slider').slider({
    slideToShow: 4,
    responsive : [
        {
            breakpoint: 767,
            settings: {
                slideToShow: 3
            }
        },
        {
            breakpoint: 450,
            settings: {
                slideToShow: 1
            }
        }
    ]
 });
 $('.testmonial-slider').slider({
     dots:true
 });