(function($){
    $.fn.slider = function(options){
        return this.each(function(){
            var sliderOptions = $.extend({
                slideToShow: 1,
                speed: 3000,
                autoplay: true,
                arrows: true,
                mouseDrag: true,
                dots: false
            },options);
            var $this = $(this);
            $this.addClass('slider-plugin');
            var sliderObj = {
                sliderBaner:$this.find('.slider-banner'),
                items: $this.find('.slider-item'),
                len: $this.find('.slider-item').length,
                nextBtn: $this.find('.next'),
                prevBtn: $this.find('.prev'),
                move: 0,
                clicked: false,
                mousedown:false,
                responsive: false,
                page_x: 0
            };
            var responsiveScreenFound = false;
            function responsiveFn(){
                if(sliderOptions.responsive){
                    $.each(sliderOptions.responsive, function(index, item){
                        if($(window).width() <= item.breakpoint){
                            $.extend(sliderOptions, item.settings);
                            responsiveScreenFound = true;
                        }else if (!responsiveScreenFound){
                            $.extend(sliderOptions, options);
                        }
                    });
                    responsiveScreenFound = false;
                }
            }
            responsiveFn();
            function resizing(){
                responsiveFn();
                sliderObj.items.outerWidth(parseInt($this.outerWidth() /  sliderOptions.slideToShow));
                sliderObj.sliderBaner.outerWidth(sliderObj.len * sliderObj.items.outerWidth());
                sliderObj.sliderBaner.css('transform', 'translateX('+ -(sliderObj.move * sliderObj.items.outerWidth())+'px)');
            }
            resizing();
            function nextFunc () {
                if(!sliderObj.clicked){
                    sliderObj.clicked = true;
                    var lastSlide = (sliderObj.len - 1) - (sliderOptions.slideToShow - 1);
                    sliderObj.move = sliderObj.move == lastSlide ? lastSlide : sliderObj.move+1;
                    sliderObj.sliderBaner.css('transform', 'translateX('+ -(sliderObj.move * sliderObj.items.outerWidth())+'px)');
                    activeDots(sliderObj.move);
                    setTimeout(function(){
                        sliderObj.clicked = false;
                        if(sliderOptions.autoplay){
                            autoplay();
                        }
                    },505);
                    if (sliderObj.move == lastSlide) {
                        sliderObj.nextBtn.addClass('disabled');
                    } else {
                        $this.find('.slider-controllers .icon').removeClass('disabled');
                    }
                }
            }
            function prevFunc(){
                if(!sliderObj.clicked){
                    sliderObj.clicked = true;
                    sliderObj.move = sliderObj.move <= 0 ? 0 : sliderObj.move-1;
                    sliderObj.sliderBaner.css('transform', 'translateX('+ -(sliderObj.move * sliderObj.items.outerWidth())+'px)');
                    activeDots(sliderObj.move);
                    setTimeout(function(){
                        sliderObj.clicked = false;
                        if(sliderOptions.autoplay){
                            autoplay();
                        }
                    },505);
                    if (sliderObj.move <= 0) {
                        sliderObj.prevBtn.addClass('disabled');
                    } else {
                        $this.find('.slider-controllers .icon').removeClass('disabled');
                    }
                }
            }
            sliderObj.nextBtn.click(nextFunc);
            sliderObj.prevBtn.click(prevFunc);
            var timeout;
            function autoplay(){
                clearTimeout(timeout);
                timeout = setTimeout(function(){
                    nextFunc();
                    autoplay();
                }, sliderOptions.speed);
            }
            if(sliderOptions.autoplay){
                autoplay();
            }

            // Dragging            
        function mouseDragging(){
            $this.on('mousedown', function(e){
                e.preventDefault();
                sliderObj.mousedown = true;
                sliderObj.page_x = e.pageX;
            });
            $(document).on('mouseup', function(e){
                e.preventDefault();
                if (sliderObj.mousedown) {
                    sliderObj.mousedown = false;
                    if(e.pageX - sliderObj.page_x > 100){
                        prevFunc();
                    }else if (e.pageX - sliderObj.page_x < -100 ){
                        nextFunc();
                    }else{
                        autoplay();
                    }
                    sliderObj.sliderBaner.css('margin-left', '0px');         
                }
            });
            $(document).on('mousemove', function(e){
                e.preventDefault();
                if (sliderObj.mousedown) {
                    sliderObj.sliderBaner.css('margin-left',(e.pageX - sliderObj.page_x) + 'px' );
                }
            });
    }
        if(sliderOptions.mouseDrag){
            mouseDragging();
        }
        //Dots
        function activeDots(index){
            $this
                .find('.slider-dots .dot')
                .removeClass('active')
                .eq(index).addClass('active');
        }
        function makeDots(){
            var df, slidesNum,i,liArr;
             liArr = [];
            slidesNum = sliderObj.len / sliderOptions.slideToShow;
            for(i = 1;i <= slidesNum;i++){
                liArr.push('<li class = \'dot ' + (i === 1 ? 'active' : '') + '\'></li>');
            }
            $this.append("<ul class ='slider-dots'> "+ liArr.join('')+"</ul>");
            $this.on('click', '.slider-dots .dot', function(){
                sliderObj.move = $(this).index();
                activeDots(sliderObj.move);
                sliderObj.sliderBaner.css('transform', 'translateX('+ -(sliderObj.move * sliderObj.items.outerWidth())+'px)');
                if(sliderOptions.autoplay){
                    autoplay();
                }
            });
        }
        if(sliderOptions.dots){
            makeDots();
        }
        // Resizing
            var winResize;
            $(window).on('resize', function(){ 
                    clearTimeout(winResize);               
                    winResize = setTimeout(resizing, 200);
            });
        });
    }
})(jQuery);

